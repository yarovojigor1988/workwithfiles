import express from 'express';
import { join } from 'path';
import formidable from 'formidable';
import * as csv from 'fast-csv';
import * as fs from 'fs';
import path from 'path';
import exphbs from 'express-handlebars';
import { writeToPath } from '@fast-csv/format';
import { fileURLToPath } from 'url';


const app = express();

const hbs = exphbs.create({
  defaultLayout : 'main',
  extname : 'hbs'
});

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

const results = [];
const __dirname = path.dirname(fileURLToPath(import.meta.url));

app.get('/', (req, res)=> {
    res.status(200);
    res.render('index');
});

app.post('/api/upload', (req, res, next) => {
    
    const form = formidable({ 
      multiples: false,
    });

    let outputFile = path.join(__dirname, 'generated' , 'output.csv');
    form.parse(req, (err, fields, file) => {
      if (err) {
        next(err);
      }

      if (file.someExpressFiles.mimetype !== 'text/csv') {
        console.log(file.someExpressFiles.mimetype);
        res.render('invalidMime');
        return
      }

      let records=[];
      let newRecords = [];

        fs.createReadStream(file.someExpressFiles.filepath)
        .pipe(csv.parse({ headers: true }))
        .on('error', error => console.error(error))
        .on('data', (data) => records.push(data))
        .on('end', () => {

            let temp = {};
            if (records.length === 0) {
              res.render('empty');
              return
            }
            records.forEach(function(item){
                if (temp[item.Email] && item.Email) { 
                     temp[item.Email].push(item);
                 } else if (item.Email){
                   temp[item.Email] = [ item ]; }   
            });

            let result = Object.keys(temp).map(function(key){ return temp[key]; });
            let maxLength = 0;
            result.forEach(el => {if (el.length > maxLength){maxLength = el.length}})

            newRecords = result.map(el=>{
                let temp = {};
                temp['Last Name'] = el[0]['Last Name'] || '';
                temp['First Name'] = el[0]['First Name'] || '';
                temp['Email Address'] = el[0]['Email'] || '';
                temp['Company'] = el[0]['Company'] || '';
                temp['Phone'] = el[0]['Phone'] || '';
                for (let i = 0; i < maxLength; i++) {
                    temp[`Address First Name - ${i+1}`] = el[i] ? el[i]['First Name'] || '' : '';
                    temp[`Address Last Name - ${i+1}`] = el[i] ? el[i]['Last Name'] || '' : '';
                    temp[`Address Company - ${i+1}`] = el[i] ? el[i]['Company'] || '' : '';
                    temp[`Address Line 1 - ${i+1}`] = el[i] ? el[i]['Address1'] || '' : '';
                    temp[`Address Line 2 - ${i+1}`] = el[i] ? el[i]['Address2'] || '' : '';
                    temp[`Address City - ${i+1}`] = el[i] ? el[i]['City'] || '' : '';
                    temp[`Address State - ${i+1}`] = el[i] ? el[i]['Province Code'] || '' : '';
                    temp[`Address Zip - ${i+1}`] = el[i] ? el[i]['Zip'] || '' : '';
                    temp[`Address Country - ${i+1}`] = el[i] ? el[i]['Country Code'] || '' : '';
                    temp[`Address Phone - ${i+1}`] = el[i] ? el[i]['Phone'] || '' : '';                    
                }
                return temp;
            });

            if (newRecords.length === 0) {
              res.render('invalidFile');
              return
            }

            writeToPath(path.resolve(outputFile), newRecords, {headers: true})
            .on('error', err => console.error(err))
            .on('finish', () => {
              console.log('Done writing.');
              res.render('done');
            });
        });      
    });
  });


const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
    console.log(`Started server on port ${PORT}`);
})

app.get('/download', function (req, res) {
  res.download(__dirname + '/generated/output.csv', 'neworders.csv');
});